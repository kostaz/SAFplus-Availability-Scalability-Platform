/**
 *  \defgroup cm Chassis Management
 *  \brief The OpenClovis Chassis Manager communicates with the chassis 
 *   to obtain and control hardware platform status information.
 */
 
//-----------------------------------------------------------

/**

\defgroup cm_intro Functional Description
\brief Description of the Chassis Management.
\ingroup cm

The OpenClovis Chassis Manager (CM) is part of the OpenClovis ASP
middleware and is the gatekeeper between ASP and the underlying
hardware platform such as an AdvancedTCA chassis or other intelligent,
manageable hardware platform. CM interfaces the underlying platform via
a Hardware Platform Interface (HPI), which is a standardized C API
specified by the Service Availability Forum (SA Forum).

The OpenClovis CM complies to the B.01.01 version of the HPI specification.
CM is integrated with and verified against two well-known implementations
of the above standard, namely:

\arg The \b OpenHPI implementation. OpenHPI is an open-source project
and supports various low-level hardware interfaces including the IPMI
and direct-IPMI used in most AdvancedTCA chassis. We recommend the use of
OpenHPI for all AdvancedTCA chassis that are integrated with Pigeon
Point Systems' (PPS) shelf manager cards (ShMM500 or ShMM300). A large
subset of the commercially available AdvancedTCA chassis use PPS shelf
manager cards. The current version of OpenHPI used and tested by
OpenClovis, Inc. is version 2.8.1.
\arg The proprietary implementation of HPI found in \b Radysis Promentum
SYS-60x0 chassis, made and distributed by Radisys Corporation.

Note that on unmanaged clusters that do not support HPI-based system
management, such as a cluster of ordinary desktop computers, the
OpenClovis CM cannot communicate with the hardware platform and therefore
is not expected to be started. This is arranged by a configuration
option used during the "configure" step (see the SDK User's Guide).

CM runs only on the system controller node(s) in ASP, and can run either
in a non-redundant mode (when there is only one system controller node
in the system), or in dual (1+1) redundant mode when there is on active
and one standby CM running.

CM depends on the Shelf Manager of the underlying hardware to monitor the
platform and to issue control operations to the platform. Specifically,
it provides the following services to ASP:

\arg It receives Hot Swap events from HPI and notifying AMF about the
arrival, imminent departure, or abrupt departure of entities that AMF
cares about.
Note that this involves mapping between two addressing domains, one
being the FRU resource ids used by HPI and the other is the node ids
or physical location information (e.g., slot number) used by ASP to
identify ASP nodes.
This mapping is not necessarily a 1:1 mapping, as the removal of a
single FRU may affect multiple ASP/system nodes.
\arg It receives sensory and other events from HPI and forwards them
to AMF or Fault Manager components of ASP, depending on the severity of
the event.
If the event is service threatening/impacting event, such as a major or
critical alarm, it is forwarded to AMF to trigger the service removal
and failover from the impacted device(s). For minor alarms the event
is passed to Fault Manager to allow optional, customized handling of
the event.
Note that this step also involves mapping from an FRU address to the
node id(s) and physical locator of affected node(s).
\arg Provides an interface for ASP to reset a node and to request
other hardware level operations.
The node address needs to be mapped back to FRU resource id by CM.
\arg It also provides a simple debug CLI command to trigger state
changes to any boards.

*/
 
//-----------------------------------------------------------

/**

\defgroup cm_usage  API Usage Examples
\brief Code Examples.
\ingroup cm

The following code snippet requests the cold reset of a blade in physical
slot 6 in chassis 0. Before the actual call, it first verifies the version
of the client library. The code relies on an \e imaginary \c handleCriticalError()
function for reporting exceptions.

\code
ClRcT rc = CL_OK;
ClVersionT version = CL_CM_VERSION;

rc = clCmVersionVerify(&version);
if (CL_OK != rc) {
    /* We simply bail out if we detect a version mismatch */
    handleCriticalError(rc, "CM service version mismatch");
}

rc = clCmBladeOperationRequest(0, 6, CL_CM_RESET_REQUEST);
if (CL_OK != rc) {
    handleCriticalError(rc, "Blade reset failed");
}
\endcode

*/

//-----------------------------------------------------------

/**
 *  \defgroup cm_error Error/Return Codes
 *  \brief Common and Component specific Error Codes.
 *  \ingroup cm
 *
 *  The majority of ASP API functions return a <b>Return Code</b> as their return value. 
 *  A zero return code (CL_OK) always indicates success. Non-zero return codes 
 *  always carry two types of information, masked into one single value: 
 *  a <b>Component Identifier</b> and an <b>Error Code</b>. The component 
 *  identifier identifies the component (or software layer) in which the problem 
 *  occurred, while the error code describes the nature of the problem. 
 *  Some error codes are common across all components, while others are 
 *  component specific. 
 *
 *  The format of the Return Codes is 0xCCNNNN (given in hex),
 *  where 0xCC is the ASP Component Identifier and 0xNNNN is the Error Code.
 *
 *  \note <b>Only the Error Codes are listed as Return values 
 *  in the Function Documentation of the API Reference pages. The real 
 *  return value also contains the Component Identifier!</b> 
 *
 *  The following links are useful while interpreting Chassis Manager 
 *  generated return codes:
 *  - \ref apirefs_errorcodes_compid_CL_CID_CM "Chassis Manager Component Identifier"
 *  - \ref apirefs_errorcodes_common
 *
 *  For further details and information about all error/return codes visit 
 *  page \ref apirefs_errorcodes.
 */

//-----------------------------------------------------------

/**
 *  \defgroup cm_apis API Reference Pages
 *  \brief Defines, Structures, Typedefs, Functions.
 *  \ingroup cm
 */

//-----------------------------------------------------------

/**
 *  \defgroup cm_aspconsole ASP Console
 *  \brief ASP Console.
 *  \ingroup cm
 */

//-----------------------------------------------------------
